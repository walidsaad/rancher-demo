********************************************************************
## Demo Getting started with Rancher
********************************************************************

This tutorial walks you through:

- Installation of Rancher 2.x
- Creation of your first cluster
- Deployment of an application, Nginx

This Lab is divided into different tasks for easier consumption.

<!-- TOC -->


1. [Provision a Linux Host](#1-provision-a-linux-host)

1. [Install Rancher](#2-install-rancher)

1. [Log In](#3-log-in)

1. [Create the Cluster](#4-create-the-cluster)

<!-- /TOC -->
<br/>
### 1. Provision a Linux Host

 Begin creation of a custom cluster by provisioning a Linux host. Your host can be:

- A cloud-host virtual machine (EC2 Instance for example)
- An on-prem VM
- A bare-metal server

  >**Note:**
  > When using a cloud-hosted virtual machine you need to allow inbound TCP communication to ports 80 and 443.  Please see your cloud-host's documentation for information regarding port configuration.
  >
  > For a full list of port requirements, refer to [Docker Installation](https://rancher.com/docs/rancher/v2.6/en/cluster-provisioning/node-requirements/).

  Provision the host according to our [Requirements](https://rancher.com/docs/rancher/v2.6/en/installation/requirements/).

### 2. Install Rancher

To install Rancher on your host, connect to it and then use a shell to install.

1.  Log in to your Linux host using your preferred shell, such as PuTTy or a remote Terminal connection.

2.  From your shell, enter the following command:

```
$ sudo docker run -d --restart=unless-stopped -p 80:80 -p 444:443 --privileged rancher/rancher
Unable to find image 'rancher/rancher:latest' locally
latest: Pulling from rancher/rancher
4f66bcc3443e: Pull complete
8180eeeb9154: Pull complete
0400565a2132: Pull complete
765a12b44120: Pull complete
f791a8a7151a: Pull complete
c66cb90bdd34: Pull complete
e9abd326df86: Pull complete
b4440f0b6ab1: Pull complete
0c9bb3c4b012: Pull complete
b945a7143662: Pull complete
fefaa13edea6: Pull complete
3f91d84ab1ed: Pull complete
28196129b54a: Pull complete
e87d010b0009: Pull complete
f95fe6ec5be5: Pull complete
da20e2e20a07: Pull complete
eda59b30ed88: Pull complete
Digest: sha256:161f0b3dabb29ce4959c17dea39d7cbc5063367419b961ad98aa42f051794273
Status: Downloaded newer image for rancher/rancher:latest
cf38d1a5af86c942470b942da40c533f8ae31ea5dc16cbcf6cf84e0c9377ce38

$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                                      NAMES
cf38d1a5af86        rancher/rancher     "entrypoint.sh"     2 minutes ago       Up About a minute   0.0.0.0:80->80/tcp, 0.0.0.0:444->443/tcp   eloquent_meitner

$ docker logs  cf38d1a5af86  2>&1 | grep "Bootstrap Password:"
2021/10/16 15:29:17 [INFO] Bootstrap Password: 67c5txf72n9hcgfht2g8bc8pqfpm5bh7pht49lvm5wqddxb7d572rm
$

```

**Result:** Rancher is installed.

### 3. Log In

Log in to Rancher to begin using the application. After you log in, you'll make some one-time configurations.

1.  Open a web browser and enter the IP address of your host: `https://<SERVER_IP>`.

    Replace `<SERVER_IP>` with your host IP address.

    ![Rancher Login](rancher/rancher-login-1.png)

    

        $ docker ps
        CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                                      NAMES
        cf38d1a5af86        rancher/rancher     "entrypoint.sh"     2 minutes ago       Up About a minute   0.0.0.0:80->80/tcp, 0.0.0.0:444->443/tcp   eloquent_meitner

        $ docker logs  cf38d1a5af86  2>&1 | grep "Bootstrap Password:"
        2021/10/16 15:29:17 [INFO] Bootstrap Password: 67c5txf72n9hcgfht2g8bc8pqfpm5bh7pht49lvm5wqddxb7d572rm
    

2.  When prompted, create a password for the default `admin` account there cowpoke!
3. Set the **Rancher Server URL**. The URL can either be an IP address or a host name. However, each node added to your cluster must be able to connect to this URL.<br/><br/>If you use a hostname in the URL, this hostname must be resolvable by DNS on the nodes you want to add to you cluster.
   ![Rancher Login](/rancher/rancher-login-2.png)

4. Welcome to Rancher !

  ![Rancher Login](rancher/rancher-login-3.png)

5. Import Your k8s Cluster to Rancher !

  ![Rancher Login](rancher/1.png)

  ![Rancher Login](rancher/2.png)

  ![Rancher Login](rancher/3.png)

6. Deploy a Monitoring Stack to Rancher !

  ![Rancher Login](rancher/4.png)

